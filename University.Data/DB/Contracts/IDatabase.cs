﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Data.DB.Contracts
{
  public interface IDatabase <T>
  { 
    T Create(T entity);
    T Read(int entityId);
    T Update(T updatedEntity);
    bool Delete(T entity);
  }
}
