﻿using System;
using System.Collections.Generic;
using System.Text;
using University.Data.Models.Contracts;

namespace University.Data.Models
{
  public class DisciplineEntity : DBEntity
  { 
    public int DisciplineId { get; set; }
    public string DisciplineName { get; set; }
  }
}
