﻿using System;
using System.Collections.Generic;
using System.Text;
using University.Data.Models.Contracts;

namespace University.Data.Models
{
  public class ProfessorEntity : DBEntity
  {
    public int ProfessorId { get; set; }
    public string Name { get; set; }
  }
}
