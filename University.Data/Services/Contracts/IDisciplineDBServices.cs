﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Data.Models;

namespace University.Data.Services.Contracts
{
  public interface IDisciplineDBServices
  {
    Task<IEnumerable<DisciplineEntity>> GetAllDisciplinesAsync();
    Task<DisciplineEntity> GetDisciplineByIdAsync(int disciplineId);
    Task<DisciplineEntity> CreateDisciplineAsync(DisciplineEntity disciplineEntity);
    Task<bool> IsDisciplineExistByIdAsync(int disciplineId);
    Task<bool> UpdateAsync(DisciplineEntity disciplineEntity);
    Task<DisciplineEntity> DeleteDesciplineAsync(int desciplineId);
  }
}
