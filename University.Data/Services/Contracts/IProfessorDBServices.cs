﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Data.Models;

namespace University.Data.Services.Contracts
{
  public interface IProfessorDBServices
  {
    Task<IEnumerable<ProfessorEntity>> GetAllProfessorsByDisciplineIdAsync(int disciplineId);
  }
}
