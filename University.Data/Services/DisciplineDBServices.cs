﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Data.Models;
using University.Data.Services.Contracts;

namespace University.Data.Services
{
  public class DisciplineDBServices : IDisciplineDBServices
  {
    public Task<DisciplineEntity> CreateDisciplineAsync(DisciplineEntity disciplineEntity)
    {
      throw new NotImplementedException();
    }

    public Task<DisciplineEntity> DeleteDesciplineAsync(int desciplineId)
    {
      throw new NotImplementedException();
    }

    public Task<IEnumerable<DisciplineEntity>> GetAllDisciplinesAsync()
    {
      throw new NotImplementedException();
    }

    public Task<DisciplineEntity> GetDisciplineByIdAsync(int disciplineId)
    {
      throw new NotImplementedException();
    }

    public Task<bool> IsDisciplineExistByIdAsync(int disciplineId)
    {
      throw new NotImplementedException();
    }

    public Task<bool> UpdateAsync(DisciplineEntity disciplineEntity)
    {
      throw new NotImplementedException();
    }
  }
}
