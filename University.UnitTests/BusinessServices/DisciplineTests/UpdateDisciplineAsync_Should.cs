﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Business.Services;
using University.Data.Models;
using University.Data.Services.Contracts;
using University.Business.Models;

namespace University.UnitTests.BusinessServices.DisciplineTests
{
  public class UpdateDisciplineAsync_Should
  {
    private Mock<IDisciplineDBServices> _disciplineDBServicesMock;
    private Mock<IProfessorDBServices> _professorDBServicesMock;
    private DisciplineServices sut;

    private const string ERROR_MESSAGE = "Oops, something went wrong!";
    private const string SUCCESS_UPDATE_MESSAGE = "Discipline successfully updated";
    private const string SUCCESS_CREATE_MESSAGE = "Discipline successfully created";
    private const string SUCCESS_DELETE_MESSAGE = "Discipline successfully deleted";

    [SetUp]
    public void Setup()
    {
      _disciplineDBServicesMock = new Mock<IDisciplineDBServices>();
      _professorDBServicesMock = new Mock<IProfessorDBServices>();
      
    }

    [TearDown]
    public void TearDown()
    {
      _disciplineDBServicesMock = null;
      _professorDBServicesMock = null;
    }

    [Test]
    public async Task ThrowArgumentNullException_WhenNullIsPassed()
    {
      // Arrange
        sut = new DisciplineServices(_disciplineDBServicesMock.Object,
                                     _professorDBServicesMock.Object);
      // Act && Assert
      var ex = Assert.ThrowsAsync<ArgumentNullException>(async () => await sut.UpdateDisciplineAsync(null)) ;
    }

    [Test]
    public async Task UpdateIfExist()
    {
      // Arrange
      int disciplineId = 987;

      _disciplineDBServicesMock.Setup(x => x.IsDisciplineExistByIdAsync(disciplineId))
                               .ReturnsAsync(true);

      string newDisciplineName = "NEW discipline Name";

      Discipline newDisciplineFake = new Discipline
      {
        DisciplineId = disciplineId,
        DisciplineName = newDisciplineName
      };

      _disciplineDBServicesMock.Setup(x => x.UpdateAsync(It.IsAny<DisciplineEntity>()))
                               .ReturnsAsync(true);

      sut = new DisciplineServices(_disciplineDBServicesMock.Object,
                                   _professorDBServicesMock.Object);


      // Act
      Discipline result = await sut.UpdateDisciplineAsync(newDisciplineFake);

      // Assert
      Assert.AreSame(newDisciplineFake, result);
      Assert.AreEqual(SUCCESS_UPDATE_MESSAGE, result.StatusMessage);
      _disciplineDBServicesMock.Verify(x => x.CreateDisciplineAsync(It.IsAny<DisciplineEntity>()), Times.Never);
      _disciplineDBServicesMock.Verify(x => x.UpdateAsync(It.IsAny<DisciplineEntity>()), Times.Once);
    }

    [Test]
    public async Task CreateIfNotExist()
    {
      // Arrange
      int disciplineId = 987;

      _disciplineDBServicesMock.Setup(x => x.IsDisciplineExistByIdAsync(disciplineId))
                               .ReturnsAsync(false);

      string disciplineName = "discipline Name";

      Discipline disciplineFake = new Discipline
      {
        DisciplineId = disciplineId,
        DisciplineName = disciplineName
      };

      DisciplineEntity disciplineEntityFake = new DisciplineEntity
      {
        DisciplineId = disciplineId,
        DisciplineName = disciplineName
      };
      _disciplineDBServicesMock.Setup(x => x.CreateDisciplineAsync(It.IsAny<DisciplineEntity>()))
                               .ReturnsAsync(disciplineEntityFake);

      sut = new DisciplineServices(_disciplineDBServicesMock.Object,
                                   _professorDBServicesMock.Object);

      // Act
      Discipline result = await sut.UpdateDisciplineAsync(disciplineFake);

      // Assert
      Assert.AreEqual(disciplineEntityFake.DisciplineName, result.DisciplineName);
      Assert.AreEqual(SUCCESS_CREATE_MESSAGE, result.StatusMessage);
      _disciplineDBServicesMock.Verify(x => x.CreateDisciplineAsync(It.IsAny<DisciplineEntity>()), Times.Once);
      _disciplineDBServicesMock.Verify(x => x.UpdateAsync(It.IsAny<DisciplineEntity>()), Times.Never);
    }
  }
}
