﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Business.Models;

namespace University.Business.Services.Contracts
{
  public interface IDisciplineServices
  {
    Task<IEnumerable<Discipline>> GetAllDisciplinesAsync();

    Task<Discipline> CreateDisciplineAsync(Discipline discipline);

    Task<Discipline> DeleteDesciplineAsync(int desciplineId);

    Task<Discipline> GetDisciplineByIdAsync(int disciplineId);
    Task<Discipline> UpdateDisciplineAsync(Discipline discipline);
  }
}
