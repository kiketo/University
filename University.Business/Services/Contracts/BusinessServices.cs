﻿using System.Threading.Tasks;
using University.Business.Contracts;
using University.Data.Models.Contracts;

namespace University.Business.Services.Contracts
{
  public abstract class BusinessServices<T, K> where T: BusinessEntity
                                               where K : DBEntity
  {
    protected abstract Task<T> MapFromDBEntityAsync(K dBentity);
    protected abstract K MapToDBEntityAsync(T businessEntity);
  }
}