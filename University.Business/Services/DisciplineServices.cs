﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using University.Business.Models;
using University.Business.Services.Contracts;
using University.Data.Models;
using University.Data.Services.Contracts;

namespace University.Business.Services
{
  public class DisciplineServices : BusinessServices<Discipline, DisciplineEntity>, IDisciplineServices
  {
    private const string ERROR_MESSAGE = "Oops, something went wrong!";
    private const string SUCCESS_UPDATE_MESSAGE = "Discipline successfully updated";
    private const string SUCCESS_CREATE_MESSAGE = "Discipline successfully created";
    private const string SUCCESS_DELETE_MESSAGE = "Discipline successfully deleted";

    private readonly IDisciplineDBServices _disciplineDBServices;
    private readonly IProfessorDBServices _professorDBServices;

    public DisciplineServices(IDisciplineDBServices disciplineDBServices, IProfessorDBServices professorDBServices)
    {
      _disciplineDBServices = disciplineDBServices ?? throw new ArgumentNullException(nameof(disciplineDBServices));
      _professorDBServices = professorDBServices ?? throw new ArgumentNullException(nameof(professorDBServices));
    }

    public async Task<Discipline> CreateDisciplineAsync(Discipline discipline)
    {
      _ = discipline ?? throw new ArgumentNullException(nameof(CreateDisciplineAsync));

      DisciplineEntity disciplineEntity = MapToDBEntityAsync(discipline);
      disciplineEntity = await _disciplineDBServices.CreateDisciplineAsync(disciplineEntity);

      if (disciplineEntity == null)
      {
        return new Discipline { StatusMessage = ERROR_MESSAGE };
      }
      discipline = await  MapFromDBEntityAsync(disciplineEntity);
      discipline.StatusMessage = SUCCESS_CREATE_MESSAGE;

      return discipline;
    }

    public async Task<Discipline> DeleteDesciplineAsync(int desciplineId)
    {
      bool disciplineEntityExist = await _disciplineDBServices.IsDisciplineExistByIdAsync(desciplineId);

      if (!disciplineEntityExist)
      {
        return new Discipline { StatusMessage = ERROR_MESSAGE };
      }

      DisciplineEntity disciplineEntity = await _disciplineDBServices.DeleteDesciplineAsync(desciplineId);
      Discipline discipline = await this.MapFromDBEntityAsync(disciplineEntity);
      discipline.StatusMessage = SUCCESS_DELETE_MESSAGE;

      return discipline;
    }

    public async Task<IEnumerable<Discipline>> GetAllDisciplinesAsync()
    {
      var disciplineEntities = await _disciplineDBServices.GetAllDisciplinesAsync();
      List<Discipline> disciplines = new List<Discipline>();
      foreach (var disciplineEntity in disciplineEntities)
      {
        Discipline discipline = await MapFromDBEntityAsync(disciplineEntity);

        disciplines.Add(discipline);
      }

      return disciplines;
    }

    public async Task<Discipline> GetDisciplineByIdAsync(int disciplineId)
    {
      DisciplineEntity disciplineEntity = await _disciplineDBServices.GetDisciplineByIdAsync(disciplineId);

      if (disciplineEntity == null)
      {
        return new Discipline { StatusMessage = ERROR_MESSAGE };
      }

      Discipline discipline = await MapFromDBEntityAsync(disciplineEntity);

      return discipline;
    }

    public async Task<Discipline> UpdateDisciplineAsync(Discipline discipline)
    {
      _ = discipline ?? throw new ArgumentNullException(nameof(CreateDisciplineAsync));

      bool disciplineEntityExist = await _disciplineDBServices.IsDisciplineExistByIdAsync(discipline.DisciplineId);

      if (disciplineEntityExist)
      {// Update
        DisciplineEntity disciplineEntity = this.MapToDBEntityAsync(discipline);
        bool updateSuccess = await _disciplineDBServices.UpdateAsync(disciplineEntity);
        if (updateSuccess)
        {
          discipline.StatusMessage = SUCCESS_UPDATE_MESSAGE;
        }
        else
        {
          discipline.StatusMessage = ERROR_MESSAGE;
        }
        return discipline;
      }

      // Create
      return await this.CreateDisciplineAsync(discipline);
      
    }

    protected override async Task<Discipline> MapFromDBEntityAsync(DisciplineEntity entity)
    {
      Discipline discipline = new Discipline
      {
        DisciplineId = entity.DisciplineId,
        DisciplineName = entity.DisciplineName,
        Professors = (await _professorDBServices.GetAllProfessorsByDisciplineIdAsync(entity.DisciplineId))
                                                .Select(p => new Professor { ProfessorName = p.Name })
                                                .ToList()
      };

      return discipline;
    }

    protected override DisciplineEntity MapToDBEntityAsync(Discipline businessEntity)
    {
      DisciplineEntity disciplineEntity = new DisciplineEntity
      {
        DisciplineId  = businessEntity.DisciplineId,
        DisciplineName = businessEntity.DisciplineName
      };

      return disciplineEntity;
    }
  }
}
