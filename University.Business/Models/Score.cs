﻿using University.Business.Contracts;

namespace University.Business.Models
{
  public class Score : BusinessEntity
  {   
    public Student Student { get; private set; }
    public Semester Semester { get; private set; }
    public Discipline Discipline { get; private set; }
    public Professor Professor { get; private set; }
    public float ScoreResult { get; private set; }
  }
}