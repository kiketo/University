﻿using System;
using System.Collections.Generic;
using System.Text;
using University.Business.Contracts;

namespace University.Business.Models
{
  public class Semester : BusinessEntity
  {
    public string Name { get; private set; } 
    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }
    public List<Score> Scores { get; private set; }
  }
}
