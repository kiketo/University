﻿using System;
using System.Collections.Generic;
using System.Text;
using University.Business.Contracts;

namespace University.Business.Models
{
  public class Student :BusinessEntity
  {
    public int Id { get; private set; }
    public string Name { get; private set;}
    public string Surname { get; private set;}
    public DateTime DOB { get; private set; }
    private List<Semester> Semesters{ get; set; }
  }
}
