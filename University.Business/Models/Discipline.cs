﻿using System.Collections.Generic;
using University.Business.Contracts;

namespace University.Business.Models
{
  public class Discipline : BusinessEntity
  { 
    public int DisciplineId { get; set; }
    public string DisciplineName { get; set; }
    public List<Professor> Professors { get; set; }
    public string StatusMessage { get; set; }
  }
}