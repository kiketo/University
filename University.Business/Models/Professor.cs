﻿using System;
using System.Collections.Generic;
using System.Text;
using University.Business.Contracts;

namespace University.Business.Models
{
  public class Professor : BusinessEntity
  {
    public int ProfessorId { get; set; }
    public string ProfessorName { get; set; }
    public List<Discipline> Disciplines { get; set; }

  }
}
