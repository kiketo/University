﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using University.Business.Models;
using University.Business.Services.Contracts;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace University.API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class DisciplineController : ControllerBase
  {
    private readonly IDisciplineServices _disciplineServices;

    public DisciplineController(IDisciplineServices disciplineServices)
    {
      _disciplineServices = disciplineServices ?? throw new ArgumentNullException(nameof(disciplineServices));
    }

    // GET: api/<Discipline>
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> GetAll()
    {
      var result = await _disciplineServices.GetAllDisciplinesAsync();

      return Ok(result);
    }

    // GET api/<Discipline>/5
    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
      Discipline result = await _disciplineServices.GetDisciplineByIdAsync(id);

      return Ok(result);
    }

    // POST api/<DisciplineController>
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] Discipline discipline)
    {
      discipline = await _disciplineServices.CreateDisciplineAsync(discipline);

      return Ok(discipline);
    }

    // PUT api/<DisciplineController>
    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Discipline discipline)
    {
      discipline = await _disciplineServices.UpdateDisciplineAsync(discipline);

      return Ok(discipline);
    }

    // DELETE api/<DisciplineController>/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
      Discipline discipline = await _disciplineServices.DeleteDesciplineAsync(id);

      return Ok(discipline);
    }
  }
}
